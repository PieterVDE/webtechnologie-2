$(document).ready(function () {
	resizeQuestions();
	//Variables
	var socket = io();
	var questionsList = $('.questions');
	var usernames;

	//Add new question
	$('#btn').on('click', function (e) {
		e.preventDefault();
		var isInput = $('#user').is("input");
		var question = null;

		// 1 - Check for username in username list
		var u = $('#user').val();
		var q = $('#question').val();
		if (u != "") {
			if (q != "") {
				var l = usernames.length;

				for (var i = 0; i < l; i++) {
					if (u === usernames[i].username) {
						//Username already in DB - use the assigned color
						var question = {
							color: usernames[i].color,
							username: u,
							question: q
						};
					}
				}

				if (isInput) {
					if (question === null) {
						//Assign random color - user not yet in DB
						var c = randomColor();
						var question = {
							color: c,
							username: u,
							question: q
						};
						usernames.push({
							color: c,
							username: u
						});
					}
					//Lock username
					$('#user').replaceWith('<p id="user">' + question.username + '</p>');
				}

				console.log('#btn click ', question);
				socket.emit('question', question);
			} else {
				alert('Please enter a question.');
			}
		} else {
			alert('Please enter a proper username.');
		}
	});

	socket.on('loadQuestions', function (questions) {
		//	console.log('loadQuestions ', questions);
		//empty questions list & usernames list
		questionsList.val('');
		usernames = [];

		//variables used in this context
		var length = questions.length;

		for (var i = 0; i < length; i++) {
			questionsList.append(
				'<li>' +
				'<div class="avatar">' +
				'<img src="https://s3.amazonaws.com/uifaces/faces/twitter/bermonpainter/128.jpg"></div>' +
				'<div class="question">' +
				'<p class="username" style="color: ' + questions[i].color + '">' + questions[i].username + '</p>' +
				'<p>' + questions[i].question + '</p>' + '</li>');
			usernames.push({
				color: questions[i].color,
				username: questions[i].username
			});
		}
	});

	//Add new message to list
	socket.on('updateQuestions', function (update) {
		console.log('Update questions ', update);
		questionsList.append(
			'<li class="newQuestion">' +
			'<div class="avatar">' +
			'<img src="https://s3.amazonaws.com/uifaces/faces/twitter/bermonpainter/128.jpg"></div>' +
			'<div class="question">' +
			'<p class="username" style="color: ' + update.color + '">' + update.username + '</p>' +
			'<p>' + update.question + '</p>' + '</li>');
		$('#question').val('');
	});
});

var resizeQuestions = function () {
	$(".questions").css('height', function () {
		return $(window).innerHeight() - $('.top-bar').height() - $('form').height() - 30;
	});
};

var resizeId;
$(window).resize(function () {
	clearTimeout(resizeId);
	resizeId = setTimeout(doneResizing, 500);
});

var doneResizing = function () {
	resizeQuestions();
};

var randomColor = function () {
	return '#' + Math.floor(Math.random() * 16777215).toString(16);
};