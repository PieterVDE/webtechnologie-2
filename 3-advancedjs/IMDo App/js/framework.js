/* ---------------------------------------- FRAMEWORK ---------------------------------- */
var WrapperElement = function (element) {
	// a wrapper element allow us to extend html dom functionality without changing the behaviour of built-in elements
	this.element = element; // this contains the actual selection
	this.isArray = (element.length > 0) ? true : false; // this allows us to see if a selection contains one or more elements
}

WrapperElement.prototype.css = function (prop, value) {
	if (this.isArray) {
		for (var i = 0; i < this.element.length; i++) {
			this.element[i].style[prop] = value;
		}
	} else {
		this.element.style[prop] = value;
	}
};

//Not sure if classList.toggle is supported on all browsers, so I made a fallback (check below)
//WrapperElement.prototype.toggleClass = function (className) {
//	if (this.isArray) {
//		// multiple elements, we'll need to loop
//		for (var i = 0; i < this.element.length; i++) {
//			this.element[i].classList.toggle(className);
//		}
//	} else {
//		this.element.classList.toggle(className);
//	}
//	return this; // return the original WrapperElement, so that we can chain multiple functions like $("li").addClass("test").toggleClass("something");
//}

WrapperElement.prototype.toggleClass = function (className) {
	var newClass = ' ' + this.element.className.replace(/[\t\r\n]/g, ' ') + ' ';
	if (this.element.className.indexOf(className) > -1) {
		while (newClass.indexOf(' ' + className + ' ') >= 0) {
			newClass = newClass.replace(' ' + className + ' ', ' ');
		}
		this.element.className = newClass.replace(/^\s+|\s+$/g, '');
	} else {
		this.element.className += ' ' + className;
	}
}

WrapperElement.prototype.addClass = function (className) {
	if (this.isArray) {
		// multiple elements, we'll need to loop
		for (var i = 0; i < this.element.length; i++) {
			this.element[i].className += " " + className;
		}
	} else {
		// just one element, so we can manipulate it without looping
		this.element.className += " " + className;
	}
	return this; // return the original WrapperElement, so that we can chain multiple functions like $("li").addClass("test").toggleClass("something");
}

WrapperElement.prototype.prepend = function (item) {
	this.element.insertBefore(item, this.element.firstChild);
}

WrapperElement.prototype.keyup = function (action) {
	if (this.isArray) {
		// multiple elements, we'll need to loop
		for (var i = 0; i < this.element.length; i++) {
			this.element[i].addEventListener('keyup', action);
		}
	} else {
		// just one element, let's go nuts
		this.element.addEventListener('keyup', action);
	}
	return this;
}

WrapperElement.prototype.click = function (action) {
	if (this.isArray) {
		for (var i = 0; i < this.element.length; i++) {
			this.element[i].addEventListener("click", action);
		}
	} else {
		this.element.addEventListener("click", action);
	}
}

WrapperElement.prototype.val = function (value) {
	if (value != undefined) {
		this.element.value = value;
	}
	return this.element.value;
}

var $ = function (selector) {
	// check if selector is an object already e.g. by passing 'this' on clicks
	if (typeof (selector) == "object") {
		return new WrapperElement(selector);
	}

	// get the first character to know if we want an element, id or class
	var firstCharacter = selector.charAt(0);
	var selectedItems;

	switch (firstCharacter) {
	case "#":
		var whatToSelect = selector.substring(1, selector.length);
		return new WrapperElement(document.getElementById(whatToSelect));
		break;
	case ".":
		var whatToSelect = selector.substring(1, selector.length);
		return new WrapperElement(document.getElementsByClassName(whatToSelect));
		break;
	default:
		return new WrapperElement(document.getElementsByTagName(selector));
		break;
	}

	var newElement = new WrapperElement(selectedItems);
	return newElement;
}