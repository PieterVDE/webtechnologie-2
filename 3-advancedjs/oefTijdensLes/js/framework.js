//Not a clean way to do this as this might change --> do it on the IMDElement
HTMLElement.prototype.imd = function () {
	alert("test");
};

function IMDElement(el) {
	this.el = el;
	this.isArray = (el[0] != undefined) ? true : false;
}

IMDElement.prototype.click = function (action) {
	if (this.isArray) {
		for (var i = 0; i < this.el.length; i++) {
			this.el[i].addEventListener("click", action);
		}
	} else {
		this.el.addEventListener("click", action);
	}
}

IMDElement.prototype.css = function (prop, value) {
	if (this.isArray) {
		for (var i = 0; i < this.el.length; i++) {
			this.el[i].style[prop] = value;
		}
	} else {
		this.el.style[prop] = value;
	}
};

//mimic jQuery functionality
function $(selector) {
	var firstChar = selector.substring(0, 1);

	switch (firstChar) {
	case "#":
		var whatToSelect = selector.substring(1, selector.length);
		return new IMDElement(document.getElementById(whatToSelect));
		break;
	case ".":
		var whatToSelect = selector.substring(1, selector.length);
		//		return new IMDElement(document.getElementById(whatToSelect));
		break;
	default:
		return new IMDElement(document.getElementsByTagName(selector));
		break;
	}
};