var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	mongoose = require('mongoose'),
	swig = require('swig'),
	bodyParser = require('body-parser'),
	path = require('path'),
	restful = require('node-restful'),
	io = require('socket.io')(http);

mongoose.connect('mongodb://localhost/imd');

app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(express.static(__dirname));

//When visiting this URL (= root), show "/views/index.html" to user
app.get('/', function (req, res) {
	res.sendFile(__dirname + '/views/index.html');
});

//Product DB entry
var Product = app.product = restful.model('Product', mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	}
}, {
	collection: 'product'
})).methods(['get', 'post', 'put', 'delete']);
Product.register(app, '/product');

//Message DB entry
var Message = app.message = restful.model('Message', mongoose.Schema({
	message: {
		type: String,
		required: true
	}
}, {
	collection: 'message'
})).methods(['get', 'post', 'put', 'delete']);
Message.register(app, '/message');

//Starting connection with socket
io.on('connection', function (socket) {
	console.log('User connected');
	socket.on('disconnect', function () {
		console.log('User disconnected');
	});

	//Add a message to DB
	socket.on('message', function (a) {
		Message.create({
			message: a
		}, function (err, b) {
			console.log('b', b);
			io.emit('updateMessages', b);
		});
	});

	//Change product amount in DB
	socket.on('updateAmount', function (product) {
		console.log(product);
		//Find product with given id
		Product.update({
			_id: product._id
		}, {
			//Set the new amount to the given amount
			$set: {
				amount: product.amount
			}
		}).exec(function (err, a) {
			console.log("Update completed.");
		});
		io.emit('updateProduct', product);
		console.log('Update complete for: ', product);
	});
});

//When a page/user connects to the app
io.on('connect', function (socket) {
	//Load all messages
	Message.find()
		.exec(function (err, messages) {
			socket.emit('loadMessages', messages);
		});

	//Load all products
	Product.find()
		.exec(function (err, products) {
			socket.emit('loadProducts', products);
		});
});

//Server setup
var server = http.listen(3003, function () {
	console.log('Server running on http://localhost:3003');
});

module.exports = app;