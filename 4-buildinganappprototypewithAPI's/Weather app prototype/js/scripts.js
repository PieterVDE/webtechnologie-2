// VARIABLES
// =========
var latitude, longitude, APICall, weatherData;
//https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE,TIME
var key = "c048e69af7641153745c44113d2b1a41";
var currentDate = "Today";

//divs
var div_currentDay = $("#currentDay");
var div_weatherInfo = $(".container-fluid");
var div_time = $("#time", div_weatherInfo);
var div_summary = $("#summary", div_weatherInfo);
var div_rain = $("#rain", div_weatherInfo);
var div_wind = $("#wind", div_weatherInfo);
var div_geoLocation = $(".geolocation");
var div_apiURL = $(".apiURL");

// FUNCTIONS
// ========
function getWeatherInfo() {
	//Either make an API call or get it from local storage
	if (localStorage.getItem("weatherData") !== null) {
		//alert("Local");
		fetchDataFromLocalStorage();
	} else if (APICall !== undefined) {
		//alert("API request");
		sendRequestToAPI();
	} else {
		console.log("YOU ARE F'ED IN THE A ON A SATURDAY.");
	}
	displayWeatherInfo();
}

function displayWeatherInfo() {
	//Current date
	div_currentDay.html(currentDate);

	//Feel temperature
	$("#feelTemp", div_summary).html("Feel temperature: " + FahrenheitToCelcius(weatherData.currently.apparentTemperature) + "°C");

	//Icon
	//clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night
	var iconSrc = "images/icons/" + weatherData.currently.icon + ".svg";
	$("#weatherIcon", div_summary).css("background-image", "url(" + iconSrc + ")");

	//Rain intensity
	$("#rainIntensity", div_rain).html("Rain intensity: " + InchToMM(weatherData.currently.precipIntensity) + "mm");

	//Rain chance
	$("#rainChance", div_rain).html("Rain chance: " + weatherData.currently.precipProbability + "%");

	//Summary
	$("#summaryText", div_summary).html(weatherData.currently.summary);

	//Temperature
	$("#actualTemp", div_summary).html(+FahrenheitToCelcius(weatherData.currently.temperature) + "°C");

	//Current time
	startTime();

	//Wind bearing
	$("#windBearing", div_wind).html("Wind bearing: " + windBearingToDirection(weatherData.currently.windBearing));

	//Wind speed
	$("#windSpeed", div_wind).html("Wind speed: " + milesToKM(weatherData.currently.windSpeed) + "km/h");
}

function sendRequestToAPI() {
	//Sends a request to the API and stores this, both in a var and local storage
	$.ajax({
		url: APICall,
		dataType: "jsonp",
		method: "GET",
		success: function (response) {
			console.log("succes!");
			weatherData = response;
			localStorage.setItem("weatherData", JSON.stringify(weatherData));
			console.log(weatherData);
		},
		error: function (e) {
			console.log("ERRER");
			console.log(e);
		}
	});
}

function fetchDataFromLocalStorage() {
	//Fetch weather data from local storage
	weatherData = JSON.parse(localStorage.getItem("weatherData"));
	console.log(weatherData);
}

function getGeoLocation() {
	//Get current location based on browser location
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			latitude = position.coords.latitude;
			longitude = position.coords.longitude;
			displayGeoLocation();
		});
	} else {
		div_geoLocation.innerHTML = "Geolocation is not supported by this browser.";
		console.log("Geolocation is not supported by this browser.");
	}
}

function displayGeoLocation() {
	//Displays the current location on the page.
	if (div_geoLocation.children().length < 2) {
		var content = "Latitude: " + latitude + " | Longitude: " + longitude;
		var p = document.createElement("p");
		p.className = "debugP";
		p.textContent = content;
		div_geoLocation.append(p);
	}
}

function getAPIUrl() {
	//Builds the URL to make the API call
	if (latitude !== null && longitude !== null) {
		APICall = "https://api.forecast.io/forecast/" + key + "/" + latitude + "," + longitude;
		displayAPIUrl();
	}
}


function displayAPIUrl() {
	//Displays the built API on the page.
	if (div_apiURL.children().length < 2) {
		var p = document.createElement("p");
		p.textContent = APICall;
		p.className = "debugP";
		div_apiURL.append(p);
	}
}

function clearDebuggingFields() {
	//Clears the debugging fields, NOT the parameters
	$(".debugP").remove();
}

function FahrenheitToCelcius(deg) {
	return Math.round(((deg - 32) / 1.8));
}

function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

function startTime() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	// add a zero in front of numbers < 10
	m = checkTime(m);
	div_time.html(h + ":" + m);
	var t = setTimeout(function () {
		startTime()
	}, 500);
}

function windBearingToDirection(bearing) {
	var windDir;
	if (bearing > 348.75 || bearing <= 11.25) windDir = "N";
	if (bearing > 11.25 || bearing <= 33.75) windDir = "NNE";
	if (bearing > 33.75 || bearing <= 56.25) windDir = "NE";
	if (bearing > 56.25 || bearing <= 78.75) windDir = "ENE";
	if (bearing > 78.75 || bearing <= 101.25) windDir = "E";
	if (bearing > 101.25 || bearing <= 123.75) windDir = "ESE";
	if (bearing > 123.75 || bearing <= 146.25) windDir = "SE";
	if (bearing > 146.25 || bearing <= 168.75) windDir = "SSE";
	if (bearing > 168.75 || bearing <= 191.25) windDir = "S";
	if (bearing > 191.25 || bearing <= 213.75) windDir = "SSW";
	if (bearing > 213.75 || bearing <= 236.25) windDir = "SW";
	if (bearing > 236.25 || bearing <= 258.75) windDir = "WSW";
	if (bearing > 258.75 || bearing <= 281.25) windDir = "W";
	if (bearing > 281.25 || bearing <= 303.75) windDir = "WNW";
	if (bearing > 303.75 || bearing <= 326.25) windDir = "NW";
	if (bearing > 326.25 || bearing <= 348.75) windDir = "NNW";
	return windDir;
}

function milesToKM(count) {
	return Math.round(count / 1.6093);
}

function InchToMM(count) {
	return count * 25.4;
}

//var Weather = function (x, y) {
//	//If no x or y are given, use current location of user
//	this.show = function (timespan) {
//		//Show the weather based on param: today, tomorrow, week
//	};
//	this.updateLocation(x, y) {
//		//Update location to show weather for based on params
//	};
//};