//XML
//<sofa>
//	<kleur>zwart</kleur>
//	<materiaal>leder</materiaal>
//</sofa>

//JSON: js object notation - This is an Object Literal
var sofa = {
	'kleur': 'zwart',
	'materiaal': 'leder'
};

//console.log(sofa.kleur);

//What if you want to re-use an object?
var sofa = function(kleur, materiaal){
	this.kleur = kleur;
	this.materiaal = materiaal;
};

var s1 = new sofa('zwart', 'leder');
var s2 = new sofa('rood', 'kunststof');

//console.log(s1.kleur + " " + s1.materiaal);
//console.log(s2.kleur + " " + s2.materiaal);

//Overerving
var relaxZetel = function(kleur, materiaal){
	this.prototype = new sofa(kleur, materiaal);
	
	this.relax = function(){
		console.log("Kom effe lekkeh zitte man!");
	}; 
}

var s3 = new relaxZetel('blauw', 'bazenstof');

console.log(s3.kleur + " " + s3.materiaal);
s3.relax();