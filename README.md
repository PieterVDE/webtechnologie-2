# Webtechnologie 2


This readme file contains a small overview of all labs made for Webtechnologie 2.

## 1 - Learn to git

[https://github.com/JasperDS/webtech-lab1](https://github.com/JasperDS/webtech-lab1 "Repo for lab 1")

The purpose of this lab was to learn the basics of working with Git. At first we had to create some simple webpages, which we had to push to a Github repo. We had to make a pull request to add the link to our repo to the list of all repo's for this week.

### What I've learned

I've been waiting for so long to finally learn how to work with Git! I've tried a few times myself, but everytime I missed something along the way so that I didn't get it to work. Now that I've had an actual lecture about Git, I understand the concept and how Git works much better.

#### Git commands
We received a cheat sheet with all frequently used git commands to get started. The commands I learned most about: 

```git clone ssh://user@domain.com/repo.git```

This command clones an existing repo from the specified URL, either using HTTP(S) or SSH.

```git status```

Simply lists the changed files in the **cwd** (current working directory).

`git add .` or ´´´git add -A´´´

Adds the changed file(s) to the next commit. This is a step I often forgot before this lecture, resulting in empty commits.

```git commit -m "This is my commit message."```

Commit the made changes (but don't push them yet!) with a specified message. Another step I forgot frequently when using Git in the past.

```git push <remote> <branch>```

Push all commits to the server. This only works if the remote directory (e.g. the directory on the server) contains no changes in comparison to the cwd **before** the commits that will be pushed. 

```git pull <remote> <branch>```

Download any changes made on the server that are not already made in your cwd. This only works if the remote and current working directory are the same before the changes are applied.

```git config --global alias.rekt push```

A little thing that has not much to do with Git functionality in particular, but which I find very amusing: I created an alias for the push command. Because of this, I no longer have to type in `git push` to push all changes to the remote directory, but I can now use `git rekt` for that. If you don't know what that means, `git rekt` is internet slang. The meaning can be found here: [Urban dictionary: rekt](http://www.urbandictionary.com/define.php?term=rekt&defid=7043615 "Urban dictionary: rekt").

## 2 - Mastering CSS animations

Seeing as I've had my fair share of practice with CSS animations, I found this lab to be rather rehearsal than learning new techniques.
I learned a couple of things, nevertheless.

### 1) Moving circles

The moving circles were pretty simple. I toggled the class for each of the three dots corresponding to their expected behaviour.

```
.down {
	transform: translateY(300px);
}
.right {
	transform: translateX(300px);
}
.downright {
	transform: translateX(300px) translateY(300px);
}
```

This class was toggled on click, meaning that they had an extra feature regarding the demo: it's also possible to stack them on top of each other again (e.g. return to default state).

### 2) Perspective and 3D rotations

This was a little harder, as working with perspective and 3D rotations was a new chunk of animations for me.

```
-webkit-transform: perspective([enter px value]);
```

The above CSS animation got me confused a few times. I did not now what the pixel value stood for. But I learned that the strength of the effect is determined by the value. The smaller the value, the closer you get from the Z plane and the more impressive the visual effect. The greater the value, the more subtle will be the effect.

I also didn't know what the difference was between `transform: perspective()` and the `perspective` property itself. I found a perfect explanation for this:

> The first gives element depth while the latter creates a 3D-space shared by all its transformed children. 

On to the 3D rotations! It was important to me to know which parameters could be passed to this property, and what their purpose is. This Stack Overflow question helped me a lot to understand how these work: http://stackoverflow.com/questions/15207351/rotate3d-shorthand

This was also the first exercise using css animations rather than transforms only. I learned there are two ways of building an animation: using `from` and `to` - for the pretty simple animations - and using timed animations based on percentages.

```
@-webkit-keyframes exampleAnimation {
	from {
		// Begin state
		// Transformation code here
	}
	to {
		// End state
		//Transformation code here
	}
}
```

```
@-webkit-keyframes exampleAnimation2 {
	0% {
		// Begin state
	}
	10% {
		// Transition state
	}
	...
	100% {
		// End state
	}
}
```

### 3) List animation

I struggled the most with this exercise, I could not get the perspective right. This resulted in the list items sliding in instead of flipping in. The rest of the exercise remained the same: use of animations, transitions and transforms to accomplish the goal.

### 4) Icon slide

In my honest opinion, this was the most fun animation to complete. At first I was a little confused as in how I should make the elements appear and dissapear off and on the screen, but once I figured that out (I used `translateY(-400%)` for this, but I'm sure there are cleaner solutions out there) the animations itself were just a matter of adding the right delays and timing functions.

#### Timing functions

A little more on the subject of timing functions. There are 5 (default) timing functions: `ease`, `ease-in`, `ease-out`, `ease-in-out` and `linear`. As you may (not) know, these are shorthand notations for defining the Bézier curve of your timing. For more information about this - and also where I got to understand this way better - I would like to leave this link here: http://cubic-bezier.com/

## 3 - Advanced JS

I liked the fact that everything except for the core stuff of this lesson (being JS) was already provided. 

I was very interested in coding our own framework, as I always wondered how jQuery (just to give an example) worked behind the scenes. Because we made this in group during the lecture, I now understand how it works and got my code to work (the biggest success for a developer).

After having this (and extending it with the functionalities I needed), it was only a matter of using this to get the IMDo App to work. An example of a functionality added to the framework, is being able to add a click event to a HTML element.

```
WrapperElement.prototype.keyup = function (action) {
	if (this.isArray) {
		// multiple elements, we'll need to loop
		for (var i = 0; i < this.element.length; i++) {
			this.element[i].addEventListener('keyup', action);
		}
	} else {
		// just one element, let's go nuts
		this.element.addEventListener('keyup', action);
	}
	return this;
}
```

## 4 - Building an app prototype with API's

The first time working with geolocation! I thought this was going to be a lot more difficult to code, but it turned out I was wrong (thanks, default browser support for this!). It's actually pretty easy getting the geolocation of a user, opening up a whole new range of possibilities for developers.

Also: working with API's! I was excited for this lecture from beforehand because being able to do so opens up alot of possibilities for future jobs, projects and assignments.

At first, I was confused about how I should start this. I worked with API's before, but setting up the connection etc. was already taken care of, resulting in me only having to use the correct functions, that's it. But initialising the API connection was new to me - and so was the data caching!

### The things I learned while working with an API

1. **You need an API key.** I learned this by having to create an account on [forecast.io](https://developer.forecast.io/).

2. **Cache data if you can.** This reduces API calls (and thus reduces your costs if you have a billing plan based on your amount of API calls) and makes for faster data access (read: a faster app!).

3. **JSON is currently the most popular data format for these kind of actions.** While I have worked with API's returning XML, I can definitely confirm API's returning data in a JSON format being faster and more efficient in their storage and data usage.

4. **There are A LOT of wind directions.** No really, A LOT. It's a common thought there are only four, but if you dig into this deeper, you'll find out there are actually 16 different wind directions (with the possibility that I have not gone into this deep enough yet).

## 5 - Node JS & MongoDB by District 01

### Notes taken during the lecture

#### 1. How to get MongoDB to work (add the `mongo` command to global path):

1. Control panel
2. System
3. Advanced System Settings
4. Environment variables
5. System variables
6. Edit "PATH"
7. Add ";" to path
8. Add directory where mongo.exe is located
9. Restart terminal window

#### 2. Stuff needed to get started:

```
> mongod
> npm init
> npm install express --save
```

#### 3. Code needed for starting a server:

```
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    mongoose = require('mongoose'),
    swig = require('swig'),
    bodyParser = require('body-parser'),
    path = require('path');
    
mongoose.connect('mongodb://localhost/imd');

app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    console.log(req);
    res.send('test2');
});

var server = http.listen(3002, function(){
	console.log('Server running on http://localhost:3002');
});

module.exports = app;
```

#### 4. Install the needed modules

```
npm install mongoose swig body-parser path http nodemon --save
```

#### 5. Continuing the process

Start the app using `> node app.js` and surf to the address specified in the terminal.
You can also start the app using `> nodemon`, which monitors made changes (but you'll have to install this first).

* Install Postman (Chrome extension)
* Also, install socket.io: `> npm install socket --save`?

This is where I kind of lost track of what to do and what he was doing. Since I got stuck at the very beginning (executing `mongod` said my dbpath was wrong) and was only able to fix it a couple of minutes later, I didn't get the chance to try everything out myself because I was too far behind.

There's a difference between io & sockets. 

> "It's not difficult, you just have to work with it regularly."
	
## 6 - An intro to AngularJS by District 01

### Notes taken during lecture

Started in 2009 - getangular.com

#### What's it all about?

* Angular = JS Framework
* Fully clientside
* SPA's (Single Page Application)
* MVC - MVVM - MVW (Model View Whatever)
* No dependencies
* Two way-binding

#### Codepen example #1

##### HTML

```
<div ng-app>
	<div>
		<input type="text" ng-model="myName" />
		<p>My name is {{myName}}</p>
	</div>
</div>
```

##### JS

```
var app = angular.module('IMDApp', []); //[] = needed dependencies
```

#### Controllers

> "Contains all your logic to feed the view."

#### Extra documentation for assignment

http://pastebin.com/01jJpy6q

##### API

###### Products

URL: http://imd.district01.be/product (PUT - GET)

PUT: 
```
	{	
		"name": "{{new product name here}}"
	}
```

###### Orders

URL: http://imd.district01.be/order (POST)

POST: 
```
{
	username: "{{ USERNAME HERE}}"
	products: [
	{
		"_id"       : "{{ PRODUCT ID }}",
		"name"		: "{{ PRODUCT NAME HERE }}",
		"quantity"	: X (int)
	},
	{
		"_id"		: "{{ PRODUCT ID }}",
		"name"		: "{{ PRODUCT NAME HERE }}",
		"quantity"	: X (int)
	}
	]
}
```

## 7 - Node, Socket.io, MongoDB & Express

Mongoose = layer between mongodb & JS to ease up storing data in the database.

Most well-known node framework: **express** (**node-restful** used in first lecture)

Node-restful uses routes to direct the different possible actions (e.g. GET, POST PUT, DEL)

#### Demo

1. Install node + express (globally)

	```
	npm install express -g (install global)
	npm install express
	npm install express -save (saves module in package.json)
	npm install express -save-dev
	```

2. Initiate starting position

	`express`

3. Install node modules: 
	
	`cd . && npm install`
	
4. Start app : 

	`nodemon`
	
5. Code stuff

	- Put routes in specified file (drinks.js)
	- Require this file in app.js
	- State when to use this file in app.js
	- Create drinks dir in view
		- Create index.jade & scoreboard.jade in ^ dir
	- Continue working in various files of the project
	- ????
	- Done! You now have a working node app with web sockets and mongoDB.
	
## 8 - SASS + BEM

Together with the Gulp lab, I was most excited for this lab. Finally, a tool to speed up development and extend the normal functionalities! I tried to learn this by myself before, but that wasn't such a big success. Therefore I was really excited about the contents of this lab.

But first, BEM. I found this method of structuring and organising CSS to be really helpful regarding my future development projects. For example, I'm planning a redesign of my portfolio after all the deadlines, so by using BEM for the CSS it'll be much easier for me to start working on the same project on a later point in time.

Up next: SASS! Comparing this to regular CSS, there are a few changes worth mentioning:

* **File structure: ** instead of dumping all of the styles in the CSS folder, we now have a separate folder called `SCSS`, which contains all the SASS styles.

* **Separate files: ** I like this approach a whole lot. Why? Because I like structure. Having all your styles in one single file may have it's advantages (f.e. you're sure the style you're searching for is declared somewhere in that particular file), but being able to structure them nicely is very satisfying to me.

* **Compression: ** SASS is able to automatically concatenate your files AND minify your code, without you needing to do this manually. Not only does this save you alot of time, it also checks for errors in your code and improves the performance of your projects. It's a win-win!

### SASS and its features

* **Variables:** Yes, using variables directly in your (S)CSS is a thing now. 

* **Nesting:** Have some HTML elements within others? instead of declaring them all separately, you can now use the following technique:

```
#container {
	p {
 		strong {
 		}
 		em {
		}
	}
} 
```

* **Mixins:** Probably one of the most exiting features in SASS - yes, aside from all the other great stuff SASS offers you. Basically, it's like being able to write functions directly in your CSS. Awesome, right? Never having to deal with vendor prefixes* again because SASS takes care of that for you (well, you still need to write the mixin).

* **Extends:** Just like in Java and other similar programming languages, SASS makes it possible to have one class extend another class and thus inheriting its styles.

* **Operators:** +, -, *, /, %. You can now use them all.

#### Frameworks

This is where the vendor prefixes asterisk is applicable. Apart from all the extra functionalities SASS already offers on top of CSS, you can even add more with SASS Frameworks. 

##### Bourbon

Bourbon is one such framework. It enables you to completely forget about manually taking care of vendor prefixes, even when the syntax of these features changes.

For more info about Bourbon: [bourbon.io](http://bourbon.io/)

## 9 - Build tools: Gulp

> "Work smarter with build tools"

### How to install gulp for a project

#### 1. Install gulp globally:

```
$ npm install --global gulp
```

#### 2. Install gulp in your project devDependencies:

```
$ npm install --save-dev gulp
```

#### 3. Create a `gulpfile.js` at the root of your project:

```
var gulp = require('gulp');

gulp.task('default', function() {
  // place code for your default task here
});
```

#### 4. Run gulp:

```
$ gulp
```

The default task will run and do nothing.

To run individual tasks, use `gulp <task> <othertask>`.